import axios from 'axios';

export default (url, method, model, id, action, requestData, headers = { 'Content-Type': 'application/json' }) => {
  let requestUrl = url;
  if (model !== null) {
    requestUrl += `/${model}`;
  }
  if (id !== null) {
    requestUrl += `/${id}`;
  }
  if (action !== null) {
    requestUrl += `/${action}`;
  }

  switch (method) {
    case 'GET':
      return axios.get(requestUrl, requestData, {
        headers,
      });
    case 'POST':
      return axios.post(requestUrl, requestData, {
        headers,
      });
    case 'DELETE':
      return axios.delete(requestUrl, {
        headers,
      });
    case 'PATCH':
      return axios.patch(requestUrl, requestData, {
        headers,
      });
    case 'PUT':
      return axios.put(requestUrl, requestData, {
        headers,
      });
    default:
      return Promise.reject(Error('Invalid Method'));
  }
};
