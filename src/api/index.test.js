import jsonServer from 'json-server';
import path from 'path';
import api  from './index';

let server = {};

const port = 3333;
const url = `http://localhost:${port}`;

describe('Test api util function works', () => {
  beforeAll((done) => {
    const app = jsonServer.create();
    const directory = 'src/api/';
    const router = jsonServer.router(path.join(directory, 'testdb.json'));
    const middlewares = jsonServer.defaults();
    app.use(middlewares);
    app.use(router);
    server = app.listen(port, () => {
      // console.log('JSON Server is running');
      done();
    });
  });

  describe('Test 1', () => {
    it('Test api get method', async (done) => {
      const response = await api(url, 'GET', 'posts', 1, null, {});
      expect(response.status).toBe(200);
      expect(response.data).toEqual({ id: 1, title: 'json-server', author: 'typicode' });
      done();
    });
  });

  describe('Test 2', () => {
    it('Test api patch and put method', async (done) => {
      let response = await api(url, 'PATCH', 'posts', 1, null, { title: 'JSON-server' });
      expect(response.status).toBe(200);
      expect(response.data).toEqual({ id: 1, title: 'JSON-server', author: 'typicode' });

      response = await api(url, 'PATCH', 'posts', 1, null, { author: 'author' });
      expect(response.status).toBe(200);
      expect(response.data).toEqual({ id: 1, title: 'JSON-server', author: 'author' });

      response = await api(url, 'PUT', 'posts', 1, null, { title: 'json-server', author: 'typicode' });
      expect(response.status).toBe(200);
      expect(response.data).toEqual({ id: 1, title: 'json-server', author: 'typicode' });

      done();
    });
  });

  describe('Test 3', () => {
    it('Test api post and delete method', async (done) => {
      let response = await api(url, 'POST', 'posts', null, null, {
        title: 'test post method',
        author: 'tester 1',
      });
      expect(response.status).toBe(201);
      expect(response.data.title).toEqual('test post method');
      expect(response.data.author).toEqual('tester 1');
      expect(response.data.id).toEqual(2);
      const postId = response.data.id;

      response = await api(url, 'DELETE', 'posts', postId, null, {});
      expect(response.status).toBe(200);

      done();
    });

    afterAll(() => {
      server.close();
    });
  });
});
